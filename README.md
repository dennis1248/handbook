## Handbook
Handbook is an application used to create online handbooks and documentation.

# Configuring handbook
This page details how to configure the handbook. This information is also available in the README.md file.
There are two files you will have to edit, <code>/config/default.php</code> and <code>/config/list.php</code>.

## Customization settings
Edit `/config/default.php` to customize various aspects of the handbook. In this file you can change the theme, default page and title among other things.
Editing this file is rather straight forward, all you have to do is change the value in the variables. If you for example want to change the title of the handbook to *How to bake cookies* you can change `$siteTitle = 'Handbook'` to `$siteTitle = 'How to bake cookies'`.

## Adding your own handbook pages
Your own handbook pages can be placed in `/content`. The name of the file has to be unique and end in `.html`.
To make your page appear in the list it has to be added to the array in `/config/list.php`. Each entry needs to have an unique name, this name can be either a numer like `1` or a string such as `"welcome page"`. The `"level"`> and `"title"` keys are required, `"level"` defines the hight of the item in the list and `"title"` defines the text used to diplay the item. `"pointer"` is used to define the name of the handbook entry to which the list entry points, this has to be set to the same as the file name of your custom handbook entry. `"pointer"` is optional, if not defined or set to a false value it will not do anything.

Here is an example;

```php
"header" => [
  "level" => 1,
  "title" => "First list entry"
  ],
"Page name" => [
  "level" => 2,
  "title" => "First page",
  "pointer" => "page_one"
  ],
"Another page name" => [
  "level" => 2,
  "title" => "Second page",
  "pointer" => "page_two"
  ]
```

This example will produce the following;
<ol>
  <li>First list entry</li>
  <ol>
    <li> <a href="#">First page</a> </li>
    <li> <a href="#">Second page</a> </li>
  </ol>
</ol>
