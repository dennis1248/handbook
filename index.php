<?php
  include $_SERVER['DOCUMENT_ROOT'].'/config/default.php';
  include $_SERVER['DOCUMENT_ROOT'].'/config/list.php';

  $page = $_GET["go"];
  $level;
  $prevLevel = 1; // Prevlevel

  // Go to default page if no page is defined in `go`
  if (empty($page)) {
    header("Location: http://".$_SERVER['HTTP_HOST']."/?go=".$defaultPage);
    exit();
  }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title><?php echo $siteTitle; ?></title>
    <link rel="stylesheet" href="/css/fonts.css">
    <link rel="stylesheet" href="/css/main.css">
    <?php
    // Select proper stylesheet
    switch ($styleSheet) {
      case 'plain':
        echo '<link rel="stylesheet" href="/css/themes/plain.css">';
        break;

      case 'void':
        echo '<link rel="stylesheet" href="/css/themes/void.css">';
        break;

      default:
        echo '<link rel="stylesheet" href="/css/themes/plain.css">';
        break;
    } ?>
  </head>
  <body>
    <div class="navbar">
      <ol class="list">
        <?php
          foreach ($index as $key => $value) {
            $level = $value[level];
            $i = $level;

            // Print `ordered list` open and close tags when switching levels
            if ($i > $prevLevel) {
              while ($i > $prevLevel) {
                echo '<ol>';
                $i--;
              }
            } elseif ($i < $prevLevel) {
              while ($i < $prevLevel) {
                echo '</ol>';
                $i++;
              }
            }

            // Set prevLevel for next loop
            $prevLevel = $level;

            if ($value[pointer] != false) {
              ?><li><a href="/?go=<?php echo $value[pointer]; ?>" class="list_link"><?php echo $value[title]; ?></a></li><?php
            } else { ?>
              <li><?php echo $value[title]; ?></li>
              <?php
            }
          }
         ?>
       </ol>
    </div>
    <div class="content">
      <div class="content_container">
        <?php include $_SERVER['DOCUMENT_ROOT']."/content/{$page}.php"; ?>
      </div>
    </div>
  </body>
</html>
