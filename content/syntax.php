<h2>Configuring handbook</h2>
<p>This page details how to configure the handbook. This information is also available in the README.md file.</p>
<p>There are two files you will have to edit, <code>/config/default.php</code> and <code>/config/list.php</code>. </p>

<h3>Customization settings</h3>
<p>Edit <code>/config/default.php</code> to customize various aspects of the handbook. In this file you can change the theme, default page and title among other things.</p>
<p>Editing this file is rather straight forward, all you have to do is change the value in the variables. If you for example want to change the title of the handbook to <i>How to bake cookies</i> you can change <code>$siteTitle = 'Handbook'</code> to <code>$siteTitle = 'How to bake cookies'</code>.</p>

<h3>Adding your own handbook pages</h3>
<p>Your own handbook pages can be placed in <code>/content</code>. The name of the file has to be unique and end in <code>.html</code>.</p>
<p>To make your page appear in the list it has to be added to the array in <code>/config/list.php</code>. Each entry needs to have an unique name, this name can be either a numer like <code>1</code> or a string such as <code>"welcome page"</code>. The <code>"level"</code> and <code>"title"</code> keys are required, <code>"level"</code> defines the hight of the item in the list and <code>"title"</code> defines the text used to diplay the item. <code>"pointer"</code> is used to define the name of the handbook entry to which the list entry points, this has to be set to the same as the file name of your custom handbook entry. <code>"pointer"</code> is optional, if not defined or set to a false value it will not do anything.</p>

<p>Here is an example;</p>
<bash>
  "header" => [
      "level" => 1,
      "title" => "First list entry"
      ],
  "Page name" => [
      "level" => 2,
      "title" => "First page",
      "pointer" => "page_one"
      ],
  "Another page name" => [
      "level" => 2,
      "title" => "Second page",
      "pointer" => "page_two"
      ]
</bash>
<p>This example will produce the following;</p>
<ol>
  <li>First list entry</li>
  <ol>
    <li> <a href="#">First page</a> </li>
    <li> <a href="#">Second page</a> </li>
  </ol>
</ol>
